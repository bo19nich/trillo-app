import React from "react";
import { ReactComponent as PlaneIcon } from "../../../assets/img/SVG/aircraft-take-off.svg";
import { ReactComponent as HomeIcon } from "../../../assets/img/SVG/home.svg";
import { ReactComponent as KeyIcon } from "../../../assets/img/SVG/key.svg";
import { ReactComponent as MapIcon } from "../../../assets/img/SVG/map.svg";
import "./styles.scss";

function SideBar() {
  return (
    <nav className="sidebar">
      <ul className="side-nav">
        <li className="side-nav__item side-nav__item--active">
          <a href="#" className="side-nav__link">
            <HomeIcon className="side-nav__icon" />
            <span>Home</span>
          </a>
        </li>
        <li className="side-nav__item">
          <a href="#" className="side-nav__link">
            <PlaneIcon className="side-nav__icon" />
            <span>Flight</span>
          </a>
        </li>
        <li className="side-nav__item">
          <a href="#" className="side-nav__link">
            <KeyIcon className="side-nav__icon" />
            <span>Car Rental</span>
          </a>
        </li>
        <li className="side-nav__item">
          <a href="#" className="side-nav__link">
            <MapIcon className="side-nav__icon" />
            <span>Tour</span>
          </a>
        </li>
      </ul>

      <div className="legal">&copy; 2020 by trillo. All rights reserved.</div>
    </nav>
  );
}

export default SideBar;

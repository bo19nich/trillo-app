import React from "react";
import { ReactComponent as LocationIcon } from "../../../assets/img/SVG/location-pin.svg";
import { ReactComponent as StarIcon } from "../../../assets/img/SVG/star.svg";
import SideBar from "../SideBar";
import "./styles.scss";

function ContentBody() {
  return (
    <div className="content">
      <SideBar />
      <main className="hotel-view">
        <div className="gallery">
          <figure className="gallery__item">
            <img
              className="gallery__photo"
              src={require("../../../assets/img/hotel-1.jpg")}
              alt="hotel-1"
            />
          </figure>
          <figure className="gallery__item">
            <img
              className="gallery__photo"
              src={require("../../../assets/img/hotel-2.jpg")}
              alt="hotel-2"
            />
          </figure>
          <figure className="gallery__item">
            <img
              className="gallery__photo"
              src={require("../../../assets/img/hotel-3.jpg")}
              alt="hotel-3"
            />
          </figure>
        </div>
        <div className="overview">
          <h1 className="overview__heading">Hotel las palms</h1>
          <div className="overview__star">
            <StarIcon className="overview__icon-star" />
            <StarIcon className="overview__icon-star" />
            <StarIcon className="overview__icon-star" />
            <StarIcon className="overview__icon-star" />
            <StarIcon className="overview__icon-star" />
          </div>
          <div className="overview__location">
            <LocationIcon className="overview__icon-location" />
            <button className="btn-inline">Abufeilar, Portugal</button>
          </div>
          <div className="overview__rating">
            <div className="overview__rating-average">8.6</div>
            <div className="overview__rating-count">468 votes</div>
          </div>
        </div>
        <div className="detail">
          <div className="description">
            <p className="paragraph">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Animi
              nisi dignissimos debitis ratione sapiente saepe. Accusantium
              cumque, quas, ut corporis incidunt deserunt quae architecto
              voluptate.
            </p>
            <p className="paragraph">
              Accusantium cumque, quas, ut corporis incidunt deserunt quae
              architecto voluptate delectus, inventore iure aliquid aliquam.
            </p>
            <ul className="list">
              <li className="list__item">Close to the Beach</li>
              <li className="list__item">Break first included</li>
              <li className="list__item">Free airport shuttle</li>
              <li className="list__item">Free wifi in all rooms</li>
              <li className="list__item">Air conditioning and heating</li>
              <li className="list__item">Pets allowed</li>
              <li className="list__item">We speak all languages</li>
              <li className="list__item">Perfect for families</li>
            </ul>
            <div className="recommend">
              <p className="recommend__count">
                Lucy and other 3 recommend this hotel
              </p>
              <div className="recommend__friends">
                <img
                  className="recommend__photo"
                  alt="friend 1"
                  src={require("../../../assets/img/user-3.jpg")}
                />
                <img
                  className="recommend__photo"
                  alt="friend 2"
                  src={require("../../../assets/img/user-4.jpg")}
                />
                <img
                  className="recommend__photo"
                  alt="friend 3"
                  src={require("../../../assets/img/user-5.jpg")}
                />
                <img
                  className="recommend__photo"
                  alt="friend 4"
                  src={require("../../../assets/img/user-6.jpg")}
                />
              </div>
            </div>
          </div>
          <div className="user-review">
            <figure className="review">
              <blockquote className="review__text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga
                doloremque architecto dicta animi, totam, itaque officia ex.
              </blockquote>
              <figcaption className="review__user">
                <img
                  src={require("../../../assets/img/user-1.jpg")}
                  className="review__photo"
                  alt="user"
                />
                <div className="review__user-box">
                  <p className="review__user-name">Nick Smith</p>
                  <p className="review__user-date">Feb 23rd, 2020</p>
                </div>
                <div className="review__rating">7.8</div>
              </figcaption>
            </figure>

            <figure className="review">
              <blockquote className="review__text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga
                doloremque architecto dicta animi.
              </blockquote>
              <figcaption className="review__user">
                <img
                  src={require("../../../assets/img/user-2.jpg")}
                  className="review__photo"
                  alt="user"
                />
                <div className="review__user-box">
                  <p className="review__user-name">Mary Thomas</p>
                  <p className="review__user-date">Sept 13th, 2020</p>
                </div>
                <div className="review__rating">9.3</div>
              </figcaption>
            </figure>

            <button className="btn-inline">
              show all <span>&rarr;</span>
            </button>
          </div>
        </div>
        <div className="cta">
          <h2 className="cta__book-now">
            Good news! We have 4 free rooms for your selected date!
          </h2>
          <button className="btn">
            <span className="btn__visible">Book now</span>
            <span className="btn__invisible">Only 4 rooms left</span>
          </button>
        </div>
      </main>
    </div>
  );
}

export default ContentBody;

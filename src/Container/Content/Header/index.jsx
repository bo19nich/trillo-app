import React from "react";
import { ReactComponent as BookmarkIcon } from "../../../assets/img/SVG/bookmark.svg";
import { ReactComponent as ChatIcon } from "../../../assets/img/SVG/chat.svg";
import { ReactComponent as SearchIcon } from "../../../assets/img/SVG/magnifying-glass.svg";
import "./styles.scss";

function Header() {
  return (
    <header className="header">
      <img
        src={require("../../../assets/img/logo.png")}
        alt="trillo logo"
        className="logo"
      />
      <form className="search">
        <input
          type="text"
          className="search__input"
          placeholder="Search hotel"
        />
        <button className="search__button">
          <SearchIcon className="search__icon" />
        </button>
      </form>
      <nav className="user-nav">
        <div className="user-nav__icon-box">
          <BookmarkIcon className="user-nav__icon" />
          <span className="user-nav__notification">7</span>
        </div>
        <div className="user-nav__icon-box">
          <ChatIcon className="user-nav__icon" />
          <span className="user-nav__notification">13</span>
        </div>
        <div className="user-nav__user">
          <img
            src={require("../../../assets/img/user.jpg")}
            alt="user"
            className="user-nav__user-photo"
          />
          <span className="user-nav__user-name">Jonas</span>
        </div>
      </nav>
    </header>
  );
}

export default Header;

import React from 'react'
import ContentBody from './Content/ContentBody'
import Header from './Content/Header'
import './styles.scss'

function index() {
    return (
        <div className="container">
            <Header />
            <ContentBody />
        </div>
    )
}

export default index
